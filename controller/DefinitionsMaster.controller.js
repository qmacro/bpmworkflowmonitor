sap.ui.define([
    "com/sap/bpm/monitorworkflow/controller/BaseMaster",
    "com/sap/bpm/monitorworkflow/model/Models",
    "com/sap/bpm/monitorworkflow/utils/Curry",
    "com/sap/bpm/monitorworkflow/utils/ErrorHandlers"
], function(BaseMaster, Models, curry, ErrorHandlers) {
    "use strict";

    return BaseMaster.extend("com.sap.bpm.monitorworkflow.controller.DefinitionsMaster", {

        onInit: function() {
            this._getRouter().getRoute("definitions").attachMatched(this._updateDefinitionsList, this);
            Models.attachWorkflowDefinitionsUpdated(this._onDefinitionsUpdated, this);
        },

        onExit: function() {
            Models.detachWorkflowDefinitionsUpdated(this._onDefinitionsUpdated, this);
            // Note: The router is already destroyed when this is called so detaching the event is not necessary
        },

        onSelectDefinition: function(oEvent) {
            if (oEvent.getParameter("selected")) {
                var definition = oEvent.getParameter("listItem").getBindingContext("wfds").getProperty("");
                this._showDefinitionDetails(definition);
            }
        },

        onSearchDefinitions: function(oEvent) {
            this._filterDefinitions(oEvent.getParameter("newValue") || "");
        },

        _showDefinitionDetails: function(definition) {
            Models.updateWorkflowDefinitionModel(definition);
            // TODO: use oRouter.navTo(...) when we can address individual items in the REST API
            this._getRouter().getTargets().display("definition", {
                definitionId: definition.id
            });
        },

        _updateDefinitionsList: function() {
            Models.updateWorkflowDefinitionsModel(null, ErrorHandlers.handleDefault);
        },

        _onDefinitionsUpdated: function() {
            this._filterDefinitions(Models.getViewModel().getProperty("/filter/definitionsList"));
            var previouslySelectedId = Models.getWorkflowDefinitionModel().getProperty("/id") || null;
            this._selectItem("definitionsList", "wfds", previouslySelectedId, "_showDefinitionDetails");
        },

        _filterDefinitions: curry(BaseMaster.prototype._filterList, "definitionsList", ["id", "name", "version"])
    });
});