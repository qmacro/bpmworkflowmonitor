sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "com/sap/bpm/monitorworkflow/config/AppConfig",
    "com/sap/bpm/monitorworkflow/utils/i18n",
    "sap/ui/Device",
    "sap/ui/model/json/JSONModel"
], function(Controller, AppConfig, I18n, Device, JSONModel) {
    "use strict";

    return Controller.extend("com.sap.bpm.monitorworkflow.controller.ExecutionLog", {

        onRecipientUsersExpand: function(oEvent) {
            var sTitle = I18n.getText("EXECUTION_LOG_ALL_RECIPIENT_USERS_TITLE");
            this._openRecipientsListPopover(oEvent.getSource(), "recipientUsers", sTitle);
        },

        onRecipientGroupsExpand: function(oEvent) {
            var sTitle = I18n.getText("EXECUTION_LOG_ALL_RECIPIENT_GROUPS_TITLE");
            this._openRecipientsListPopover(oEvent.getSource(), "recipientGroups", sTitle);
        },

        isNonEmptyArray: function(aArray) {
            return Array.isArray(aArray) && aArray.length > 0;
        },

        formatAbbreviatedRecipients: function(aRecipients) {
            if (this.isNonEmptyArray(aRecipients)) {
                return aRecipients.slice(0, AppConfig.NumAbbreviatedRecipients).join(", ");
            }
            return "";
        },

        isRecipientsFallbackRequired: function(aRecipientUsers, aRecipientGroups) {
            return !this.isNonEmptyArray(aRecipientUsers) && !this.isNonEmptyArray(aRecipientGroups);
        },

        hasAdditionalRecipients: function(aRecipients) {
            return Array.isArray(aRecipients) && aRecipients.length > AppConfig.NumAbbreviatedRecipients;
        },

        formatAdditionalRecipientsLink: function(aRecipients) {
            if (Array.isArray(aRecipients)) {
                return I18n.getText("EXECUTION_LOG_TYPE_USERTASK_CREATED_MORE_USERS", aRecipients.length -
                    AppConfig.NumAbbreviatedRecipients);
            }
            return "";
        },

        _openRecipientsListPopover: function(oSource, sProperty, sTitle) {
            var oBindingContext = oSource.getBindingContext("wfilog");
            var aUsers = oSource.getModel("wfilog").getProperty(sProperty, oBindingContext);
            this._createPopoverFromFragment("RecipientsPopover");
            if (!Device.system.phone) {
                aUsers = aUsers.slice(AppConfig.NumAbbreviatedRecipients);
            }
            var oPopoverModel = new JSONModel();
            oPopoverModel.setSizeLimit(100);
            oPopoverModel.setData({
                title: sTitle,
                list: aUsers
            });
            this._oPopover.setModel(oPopoverModel, "recipients");
            oSource.addDependent(this._oPopover);
            this._oPopover.openBy(oSource);
        },

        _createPopoverFromFragment: function(sFragmentName) {
            if (!this._oPopover) {
                this._oPopover = sap.ui.xmlfragment("com.sap.bpm.monitorworkflow.view.fragments." + sFragmentName);
            }
        }

    });
});