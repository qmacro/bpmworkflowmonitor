sap.ui.define([
    "com/sap/bpm/monitorworkflow/controller/BaseMaster",
    "com/sap/bpm/monitorworkflow/model/Models",
    "com/sap/bpm/monitorworkflow/utils/Curry",
    "com/sap/bpm/monitorworkflow/utils/ErrorHandlers"
], function(BaseMaster, Models, curry, ErrorHandlers) {
    "use strict";

    return BaseMaster.extend("com.sap.bpm.monitorworkflow.controller.InstancesMaster", {

        onInit: function() {
            this._getRouter().getRoute("instances").attachMatched(this._updateInstancesList, this);
            Models.attachWorkflowInstancesUpdated(this._onInstancesUpdated, this);
        },

        onExit: function() {
            Models.detachWorkflowInstancesUpdated(this._onInstancesUpdated, this);
            // Note: The router is already destroyed when this is called so detaching the event is not necessary
        },

        onSelectInstance: function(oEvent) {
            var oInstance = oEvent.getParameter("listItem").getBindingContext("wfis").getProperty("");
            this._showInstanceDetails(oInstance, true);
        },

        onSearchInstances: function(/* oEvent */) {
            this._updateInstancesList();
        },

        onMorePress: function(/* oEvent */) {
            var top = Models.getViewModel().getProperty("/top/instancesList");
            Models.getViewModel().setProperty("/top/instancesList", top + Models.getPageSize());
            this._updateInstancesList();
        },

        _updateInstancesList: function() {
            Models.updateWorkflowInstancesModel(null, ErrorHandlers.handleDefault);
        },

        _onInstancesUpdated: function(oEvent) {
            var oInstances = oEvent.getParameter("instances");
            Models.getViewModel().setProperty("/count/instancesList", oInstances.length);
            var previouslySelectedId = Models.getWorkflowInstanceModel().getProperty("/id") || null;
            this._selectItem("instancesList", "wfis", previouslySelectedId, "_showInstanceDetailsWithoutRefresh");
        },

        _showInstanceDetailsWithoutRefresh: function(oInstance) {
            this._showInstanceDetails(oInstance, false);
        },

        _showInstanceDetails: function(oInstance, bRefreshInstance) {
            this._getRouter().getTargets().display("instance", {
                instanceId: oInstance.id
            });
            sap.ui.getCore().getEventBus().publish("InstancesMaster", "ShowInstanceDetails", {
                instance: oInstance,
                refresh: !!bRefreshInstance
            });
        }
    });
});