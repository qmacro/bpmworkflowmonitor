sap.ui.define([
    "com/sap/bpm/monitorworkflow/controller/BaseDetail",
    "com/sap/bpm/monitorworkflow/model/Models",
    "com/sap/bpm/monitorworkflow/utils/Formatter",
    "com/sap/bpm/monitorworkflow/utils/i18n",
    "com/sap/bpm/monitorworkflow/utils/ErrorHandlers",
    "com/sap/bpm/monitorworkflow/model/ExecutionLogProcessor",
    "com/sap/bpm/monitorworkflow/utils/Curry",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function(BaseDetail, Models, Formatter, I18n, ErrorHandlers, ExecutionLogProcessor, curry, MessageBox, MessageToast) {
    "use strict";

    return BaseDetail.extend("com.sap.bpm.monitorworkflow.controller.InstancesDetail", {

        onInit: function() {
            var oEventBus = sap.ui.getCore().getEventBus();
            oEventBus.subscribe("InstancesMaster", "ShowInstanceDetails", this._showInstanceDetails, this);
        },

        onExit: function() {
            var oEventBus = sap.ui.getCore().getEventBus();
            oEventBus.unsubscribe("InstancesMaster", "ShowInstanceDetails", this._showInstanceDetails, this);
        },

        onRefreshInstance: function() {
            var oInstance = Models.getWorkflowInstanceModel().getProperty("/");
            this._refreshSingleInstance(oInstance);
            this._refreshInstanceDetails();
        },

        _showInstanceDetails: function(sChannel, sEventId, oEventData) {
            // The event provides two pieces of information:
            //  * instance: The workflow instance to be shown
            //  * refresh: Whether to refresh the model by calling the backend with a single instance ID
            if (oEventData.refresh) {
                this._refreshSingleInstance(oEventData.instance);
            } else {
                Models.setWorkflowInstanceModelData(oEventData.instance);
            }
            this._refreshInstanceDetails();
        },

        _refreshSingleInstance: function(oInstance) {
            Models.updateWorkflowInstanceModel(oInstance, function(oUpdatedInstance) {
                // Find and replace the instance in the master list model (in order to reflect the current status)
                var oInstancesModel = Models.getWorkflowInstancesModel();
                var aInstanceList = oInstancesModel.getData();
                var aNewInstancesList = jQuery.map(aInstanceList, function(oCurrentInstance) {
                    if (oCurrentInstance.id === oUpdatedInstance.id) {
                        return oUpdatedInstance;
                    } else {
                        return oCurrentInstance;
                    }
                });
                oInstancesModel.setData(aNewInstancesList);
            }, function(oError) {
                var aHandlers = ErrorHandlers.create(
                    ErrorHandlers.handleSilentError,
                    ErrorHandlers.handleSessionTimeout,
                    ErrorHandlers.handleInternalServerError,
                    curry(ErrorHandlers.handleOperationFailed, "GET_INSTANCE")
                );
                ErrorHandlers.handle(aHandlers, oError);
            });
        },

        _refreshInstanceDetails: function() {
            // Load error messages
            Models.updateWorkflowInstanceErrorsModelToCurrentWorkflowInstance(null, function(oError) {
                var aHandlers = ErrorHandlers.create(
                    ErrorHandlers.handleSilentError,
                    ErrorHandlers.handleSessionTimeout,
                    ErrorHandlers.handleInternalServerError,
                    curry(ErrorHandlers.handleOperationFailed, "GET_MESSAGES")
                );
                ErrorHandlers.handle(aHandlers, oError);
            });

            // Load workflow execution logs
            var oWarningText = this.byId("executionLogWarning");
            oWarningText.setVisible(false);
            Models.updateWorkflowInstanceExecutionLogsModelToCurrentWorkflowInstance(
                function(newData /*, oldData */) {
                    ExecutionLogProcessor.basicGrouping(newData, true);
                    return newData;
                },
                function(newData) {
                    oWarningText.setVisible(!ExecutionLogProcessor.validate(newData));
                },
                function(oError) {
                    var aHandlers = ErrorHandlers.create(
                        ErrorHandlers.handleSilentError,
                        ErrorHandlers.handleSessionTimeout,
                        ErrorHandlers.handleInternalServerError,
                        curry(ErrorHandlers.handleOperationFailed, "GET_EXECUTION_LOGS")
                    );
                    ErrorHandlers.handle(aHandlers, oError);
                }
            );
        },

        onCancelWorkflowInstance: function(/* oEvent */) {
            var oModel = Models.getWorkflowInstanceModel();
            var sSubject = Formatter.formatWorkflowInstanceTitle(
                oModel.getProperty("/subject"), oModel.getProperty("/definitionId"), oModel.getProperty("/startedAt")
            );
            var sConfirmationMessage = I18n.getText("INSTANCES_DETAIL_CANCEL_CONFIRMATION_MESSAGE", [sSubject]);
            var sConfirmationTitle = I18n.getText("INSTANCES_DETAIL_CANCEL_CONFIRMATION_TITLE");
            var sConfirmButton = I18n.getText("INSTANCES_DETAIL_CANCEL_CONFIRMATION_CONFIRM");
            var sAbortButton = I18n.getText("INSTANCES_DETAIL_CANCEL_CONFIRMATION_CLOSE");

            MessageBox.confirm(sConfirmationMessage, {
                title: sConfirmationTitle,
                actions: [sConfirmButton, sAbortButton],
                onClose: function(sAction) {
                    if (sAction === sConfirmButton) {
                        this._cancelWorkflowInstance();
                    }
                }.bind(this)
            });
        },

        _cancelWorkflowInstance: function() {
            Models.cancelCurrentWorkflowInstance(function() {
                MessageToast.show(I18n.getText("INSTANCES_DETAIL_CANCEL_SUCCESS"));
                Models.updateWorkflowInstancesModel(null, ErrorHandlers.handleDefault);
            }, function(oError) {
                this._handleActionError("CANCEL_WORKFLOW", oError);
            }.bind(this));
        },

        onRetryWorkflowInstance: function(/* oEvent */) {
            Models.runCurrentWorkflowInstance(function() {
                MessageToast.show(I18n.getText("INSTANCES_DETAIL_RETRY_SUCCESS"));
                this.onRefreshInstance();
            }.bind(this), function(oError) {
                this._handleActionError("RETRY_WORKFLOW", oError);
            }.bind(this));
        },

        onSuspendWorkflowInstance: function(/* oEvent */) {
            Models.suspendCurrentWorkflowInstance(function() {
                MessageToast.show(I18n.getText("INSTANCES_DETAIL_SUSPEND_SUCCESS"));
                this.onRefreshInstance();
            }.bind(this), function(oError) {
                this._handleActionError("SUSPEND_WORKFLOW", oError);
            }.bind(this));
        },

        onResumeWorkflowInstance: function(/* oEvent */) {
            Models.runCurrentWorkflowInstance(function() {
                MessageToast.show(I18n.getText("INSTANCES_DETAIL_RESUME_SUCCESS"));
                this.onRefreshInstance();
            }.bind(this), function(oError) {
                this._handleActionError("RESUME_WORKFLOW", oError);
            }.bind(this));
        },

        _handleActionError: function(sAction, oError) {
            var aHandlers = ErrorHandlers.create(
                curry(ErrorHandlers.handleInstanceNotFound, sAction),
                ErrorHandlers.handleSessionTimeout,
                ErrorHandlers.handleInvalidCSRFToken,
                ErrorHandlers.handleNoPermission,
                curry(ErrorHandlers.handleOperationFailed, sAction)
            );
            ErrorHandlers.handle(aHandlers, oError);
        },

        onShowDefinition: function(/* oEvent */) {
            Models.getViewModel().setProperty("/filter/definitionsList", Models.getWorkflowInstanceModel().getProperty("/definitionId"));
            this._getRouter().navTo("definitions");
        }
    });
});