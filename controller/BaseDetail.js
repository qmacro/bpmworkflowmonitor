sap.ui.define([
    "com/sap/bpm/monitorworkflow/controller/Base",
    "com/sap/bpm/monitorworkflow/utils/Curry",
    "com/sap/bpm/monitorworkflow/utils/Utils"
], function(Base, curry, Utils) {
    "use strict";

    return Base.extend("com.sap.bpm.monitorworkflow.controller.BaseDetail", {

        onNavButtonPress: function(/* oEvent */) {
            // TODO: use history.go(-1) once we can address individual items in the REST API
            // window.history.go(-1);

            // On a phone, splitApp.getCurrentMasterPage() returns the Instances/DefinitionsDetail
            // as the current master page, so we need to work arount this behavior
            var splitApp = this.getView().getParent().getParent();
            var currViewName = splitApp.getCurrentMasterPage().getViewName();
            var masterPages = splitApp.getMasterPages();
            var targetPage = null;

            function filterMasterPage(regex, masterPage) {
                return masterPage.getViewName().search(regex) === 0;
            }

            if (currViewName.search(/.*InstancesDetail.*/) === 0) {
                targetPage = Utils.arrayFind(masterPages, curry(filterMasterPage, /.*InstancesMaster.*/));
            } else {
                targetPage = Utils.arrayFind(masterPages, curry(filterMasterPage, /.*DefinitionsMaster.*/));
            }

            splitApp.toMaster(targetPage);
        }
    });
});