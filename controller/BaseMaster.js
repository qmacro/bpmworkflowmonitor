sap.ui.define([
    "com/sap/bpm/monitorworkflow/controller/Base",
    "com/sap/bpm/monitorworkflow/model/Models",
    "sap/ui/model/Filter",
    "com/sap/bpm/monitorworkflow/utils/Utils"
], function(Base, Models, Filter, Utils) {
    "use strict";

    return Base.extend("com.sap.bpm.monitorworkflow.controller.BaseMaster", {

        _filterList: function(sListId, aFilterBy, sQuery) {
            // Construct a filter RegExp that compares case-insensitively
            var oRegex = new RegExp(jQuery.sap.escapeRegExp(sQuery), "i");
            var fnFilter = oRegex.test.bind(oRegex);

            // Create one filter per searchable property
            var aFilters = [];
            aFilterBy.forEach(function(sProperty) {
                aFilters.push(new Filter(sProperty, fnFilter));
            });

            // Filter the list by combining all filters using OR
            var oList = this.byId(sListId);
            var oBinding = oList.getBinding("items");
            var numItems = oBinding.filter(new Filter(aFilters, false)).getLength();
            Models.getViewModel().setProperty("/count/" + sListId, numItems);
        },

        _selectItem: function(listId, modelName, previouslySelectedId, sShowDetailsMethodName) {
            var oList = this.byId(listId),
                aListItems = oList.getItems(),
                oSelectListItem;
            if (aListItems.length === 0) {
                this._getRouter().getTargets().display("noSelection");
                return;
            }
            if (previouslySelectedId) {
                oSelectListItem = Utils.arrayFind(aListItems, function(oItem) {
                    return oItem.getBindingContext(modelName).getProperty("id") === previouslySelectedId;
                });
            }
            if (!oSelectListItem) {
                oSelectListItem = aListItems[0];
            }
            oList.setSelectedItem(oSelectListItem, true);
            var oInstanceData = oSelectListItem.getBindingContext(modelName).getProperty("");
            this[sShowDetailsMethodName](oInstanceData);
        }
    });
});