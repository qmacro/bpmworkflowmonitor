sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "com/sap/bpm/monitorworkflow/utils/Formatter"
], function(Controller, UIComponent, Formatter) {
    "use strict";

    return Controller.extend("com.sap.bpm.monitorworkflow.controller.Base", {

        formatter: Formatter,

        _getRouter: function() {
            return UIComponent.getRouterFor(this);
        }

    });
});