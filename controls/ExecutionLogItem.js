sap.ui.define([
    "com/sap/bpm/monitorworkflow/utils/i18n", "com/sap/bpm/monitorworkflow/model/Models", "sap/ui/core/Control", "sap/m/Text",
    "sap/ui/layout/FixFlex", "sap/m/Label", "sap/m/Link", "sap/m/HBox", "sap/m/VBox"
], function(I18n, Models, Control, Text, FixFlex,
    Label, Link, HBox, VBox
) {
    "use strict";

    return Control.extend("com.sap.bpm.monitorworkflow.controls.ExecutionLogItem", {

        metadata: {
            properties: {
                eventType: {
                    type: "string",
                    defaultValue: "None"
                },
                recipientUsers: {
                    type: "Array",
                    defaultValue: []
                }
            },
            aggregations: {
                content: {
                    type: "sap.ui.core.Control",
                    multiple: false
                }
            },
            defaultAggregation: "content"
        },

        init: function() {
        },

        _createPopoverFromFragment: function(sFragment, fnGenerator) {
            if (!this._oPopover) {
                this._oPopover = sap.ui.xmlfragment("com.sap.bpm.monitorworkflow.view.fragments." + sFragment);
                if (typeof fnGenerator === "function") {
                    fnGenerator.call(this, this._oPopover);
                }
                this.addDependent(this._oPopover);
            }
        },

        _createServiceEndpointControl: function() {
            var oContainer = new HBox({
                wrap: "Wrap",
                renderType: "Bare"
            });

            var oLink = new Link({
                wrapping: true
            });
            oLink.bindProperty("text", {
                path: "wfilog>restEndpoint/destinationName",
                formatter: function(sDestinationName) {
                    return sDestinationName || I18n.getText("DESTINATION_NAME_UNKNOWN");
                }
            });
            oLink.bindProperty("enabled", {
                path: "wfilog>restEndpoint/httpMethod",
                formatter: function(sHttpMethod) {
                    return !!sHttpMethod;
                }
            });
            oLink.attachPress(function(oEvent) {
                this._createPopoverFromFragment("DestinationDetails", function(oPopover) {
                    oPopover.bindElement("wfilog>restEndpoint");
                });
                this._oPopover.openBy(oEvent.getSource());
            }, this);

            var oLabel = new Label({
                text: "{i18n>SERVICE_TASK_ENDPOINT_LABEL}"
            });
            oLabel.addStyleClass("sapUiTinyMarginEnd");

            oContainer.addItem(oLabel);
            oContainer.addItem(oLink);
            return oContainer;
        },

        _createErrorMessageControl: function() {
            return this._createLabelWithText(
                "{i18n>ACTIVITY_FAILURE_ERROR_LABEL}",
                "{= ${wfilog>error/message} || ${i18n>ERROR_MESSAGE_UNKNOWN}}"
            );
        },

        _createUserListControl: function() {
            return sap.ui.xmlfragment("com.sap.bpm.monitorworkflow.view.fragments.Recipients", sap.ui
                .controller("com.sap.bpm.monitorworkflow.controller.ExecutionLog"));
        },

        _createIMETextControl: function(textId) {
            return this._createLabelWithText(I18n.getText(textId), "{wfilog>messageName}");
        },

        _createTypedBTETextControl: function(sEventType) {
            var sBteType = sEventType.substring(0, sEventType.indexOf("_"));
            var sI18nTypedBteKey = "EXECUTION_LOG_" + sBteType + "_BOUNDARY_TIMER_EVENT_NAME_LABEL";
            return this._createLabelWithText(I18n.getText(sI18nTypedBteKey), "{wfilog>activityName}");
        },

        _createUntypedBTETextControl: function() {
            return this._createLabelWithText(I18n.getText("EXECUTION_LOG_BOUNDARY_TIMER_EVENT_NAME_LABEL"), "{wfilog>boundaryEventName}");
        },

        _createUntypedBTEDurationControl: function() {
            return this._createLabelWithText(I18n.getText("EXECUTION_LOG_BOUNDARY_TIMER_EVENT_NAME_LABEL"), "{wfilog>cause}");
        },

        _createDurationTextControl: function() {
            return this._createLabelWithText(I18n.getText("EXECUTION_LOG_DURATION_LABEL"), "{wfilog>duration}");
        },

        _createTimePassedTextControl: function() {
            return this._createLabelWithText(I18n.getText("EXECUTION_LOG_TIME_PASSED_LABEL"), "{wfilog>duration}");
        },

        _createLabelWithText: function(label, text, formatter) {
            var oContainer = new HBox({
                wrap: "Wrap",
                renderType: "Bare"
            });

            var oLabel = new Label({
                text: label
            });
            var oText;
            if (formatter) {
                oText = new Text({});
                oText.bindProperty("text", {
                    path: "wfilog>changes/update",
                    formatter: formatter
                });
            } else {
                oText = new Text({
                    text: text
                });
            }
            oLabel.addStyleClass("sapUiTinyMarginEnd");

            oContainer.addItem(oLabel);
            oContainer.addItem(oText);
            return oContainer;
        },

        _createOptionalLabelWithText : function(label, text) {
            var formatter = function(sValue) {
                return sValue !== undefined;
            };
            var path = text.replace('{', '').replace('}', '');

            var oContainer = new HBox({
                wrap : "Wrap",
                renderType: "Bare"
            });
            oContainer.bindProperty("visible", {
                path : path,
                formatter : formatter
            });

            var oLabel = new Label({
                text : label
            });
            oLabel.addStyleClass("sapUiTinyMarginEnd");

            var oText = new Text({
                text : text
            });

            oContainer.addItem(oLabel);
            oContainer.addItem(oText);
            return oContainer;
        },

        setEventType: function(sEventType) {
            this.setProperty("eventType", sEventType, true);
            this.destroyContent();
            if (this._oPopover) {
                this._oPopover.destroy();
                delete this._oPopover;
            }
            if (sEventType === "SERVICETASK_COMPLETED") {
                this.setContent(new FixFlex({
                    vertical: false,
                    fixFirst: false,
                    minFlexSize: 1,
                    fixContent: new Text({
                        width: "70px",
                        text: "{wfilog>executionDurationText}",
                        textAlign: "End"
                    }),
                    flexContent: this._createServiceEndpointControl()
                }));
            } else if (sEventType === "SERVICETASK_FAILED") {
                var oContainer = new VBox();
                oContainer.addItem(this._createServiceEndpointControl());
                oContainer.addItem(this._createErrorMessageControl());
                this.setContent(oContainer);
            } else if (sEventType === "SCRIPTTASK_FAILED") {
                this.setContent(this._createErrorMessageControl());
            } else if (sEventType === "USERTASK_CREATED") {
                var oContainer = new VBox();
                oContainer.addItem(this._createUserListControl());
                oContainer.addItem(this._createOptionalLabelWithText(I18n.getText("EXECUTION_LOG_INITIATOR_LABEL"), "{wfilog>initiatorId}"));
                this.setContent(oContainer);
            } else if (sEventType === "USERTASK_FAILED") {
                this.setContent(this._createErrorMessageControl());
            } else if (sEventType === "USERTASK_PATCHED_BY_ADMIN") {
                var oContainer = new VBox();
                oContainer.addItem(this._createOptionalLabelWithText(I18n.getText("EXECUTION_LOG_SUBJECT_CHANGED_PROPERTIES_LABEL"), "{wfilog>newSubject}"));
                oContainer.addItem(this._createLabelWithText(I18n.getText("EXECUTION_LOG_CHANGED_PROPERTIES_LABEL"), "{wfilog>changes/update}",
                    function(aItems) {
                        return aItems.join(", ");
                    }
                ));
                this.setContent(oContainer);
            } else if (sEventType === "INTERMEDIATE_MESSAGE_EVENT_REACHED") {
                this.setContent(this._createIMETextControl("EXECUTION_LOG_TYPE_INTERMEDIATE_MESSAGE_EVENT_REACHED_TEXT"));
            } else if (sEventType === "INTERMEDIATE_MESSAGE_EVENT_TRIGGERED") {
                this.setContent(this._createIMETextControl("EXECUTION_LOG_TYPE_INTERMEDIATE_MESSAGE_EVENT_TRIGGERED_TEXT"));
            } else if (jQuery.sap.endsWith(sEventType, "BOUNDARY_TIMER_EVENT_TRIGGERED")) {
                var oContainer = new VBox();
                oContainer.addItem(this._createTypedBTETextControl(sEventType));
                oContainer.addItem(this._createTimePassedTextControl());
                this.setContent(oContainer);
            } else if (sEventType === "USERTASK_CANCELED_BY_BOUNDARY_EVENT") {
                this.setContent(this._createUntypedBTETextControl());
            } else if (sEventType === "INTERMEDIATE_TIMER_EVENT_REACHED") {
                this.setContent(this._createDurationTextControl());
            } else if (sEventType === "INTERMEDIATE_TIMER_EVENT_TRIGGERED") {
                this.setContent(this._createTimePassedTextControl());
            }
            
            /**
             * Add label showing event type if we can. Following the VBox-as-container pattern here.
             * First, we'll create a VBox with a Label/Text showing the event type, optionally 
             * adding anothe Label/Text combo to show the task ID if appropriate. 
             * Then:
             * - if there's no content, we'll set that new VBox as the content
             * - if there's content but it's not a VBox, we'll set the new VBox as content,
             *   adding the existing content to it
             * - otherwise it's a VBox so just add the new Label/Text to that
             */
            var oTypeInfo = this._createOptionalLabelWithText(I18n.getText("EVENT_TYPE_LABEL"), "{wfilog>type}"),
            	oTaskIdInfo = this._createOptionalLabelWithText(I18n.getText("TASK_ID_LABEL"), "{wfilog>taskId}"),
	            oVBox = new VBox({ items : [ oTypeInfo ] }),
            	oContentControl = this.getContent();
            if (!oContentControl) {
            	this.setContent(oVBox);
            } else if (oContentControl.getMetadata().getName() === "sap.m.VBox") {
                oContentControl.addItem(oTypeInfo);
                oVBox = oContentControl;
            } else {
            	this.setContent(oVBox.addItem(oContentControl));
            }
            if (sEventType === "USERTASK_CREATED") {
            	oVBox.addItem(oTaskIdInfo);
            }
            
        },

        renderer: function(oRM, oControl) {
            oRM.write("<div");
            oRM.writeControlData(oControl);
            oRM.writeClasses();
            oRM.write(">");
            oRM.renderControl(oControl.getAggregation("content"));
            oRM.write("</div>");
        }
    });
});
