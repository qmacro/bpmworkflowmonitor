sap.ui.define([
    "sap/ui/base/EventProvider",
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device",
    "com/sap/bpm/monitorworkflow/utils/Curry"
], function(EventProvider, JSONModel, Device, curry) {
    "use strict";

    var models = {
            deviceModel: null,
            viewModel: null,
            workflowDefinitionsModel: null,
            workflowDefinitionModel: null,
            workflowInstancesModel: null,
            workflowInstanceModel: null,
            workflowInstanceContext: null,
            workflowInstanceErrorsModel: null,
            workflowInstanceExecutionLogsModel: null
        },
        csrfToken = null,
        modelAPISingleton = {},
        pageSize = 100;

    // Globally adds CSRF header for modifying requests
    jQuery.ajaxPrefilter(function(options, originalOptions, jqXhr) {
        if (csrfToken && !options.crossDomain && (
                options.type === "PUT" ||
                options.type === "POST" || options.type === "PATCH" || options.type === "DELETE"
            )) {
            return jqXhr.setRequestHeader("X-CSRF-Token", csrfToken);
        }
    });

    function isCSRFRequired(jqXhr) {
        return jqXhr.status === 403 && jqXhr.getResponseHeader("X-CSRF-Token") === "Required";
    }

    function retryWithNewCSRFToken(bPreventRetry, fnRetry, onError, jqXhr) {
        if (isCSRFRequired(jqXhr)) {
            if (bPreventRetry) {
                return {
                    errorCode: "csrfValidationFailed"
                };
            } else {
                csrfToken = null;
                modelAPISingleton.requestCSRFToken(fnRetry, onError);
                return {
                    // Don't report errors to error handlers until the retry is done
                    silent: true
                };
            }
        }
    }

    function isSessionExpired(jqXhr) {
        return jqXhr.getResponseHeader("com.sap.cloud.security.login") === "login-request";
    }

    function handleSuccessAndSessionExpire(onSuccess, onExpire, oData, sTextStatus, jqXhr) {
        if (isSessionExpired(jqXhr)) {
            jQuery.sap.log.error("REST API call encountered a session timeout with HTTP status code "
                + jqXhr.status + ":\n" + JSON.stringify(jqXhr.responseText));
            onExpire({
                errorCode: "bpm.wfadmin.session.expired",
                httpStatus: jqXhr.status,
                jqXhr: jqXhr
            });
        } else if (onSuccess) {
            onSuccess(oData, sTextStatus, jqXhr);
        }
    }

    function handleRESTError(aHandlers, jqXhr, sErrorType) {
        var sErrorCode = "",
            sErrorMessage = "",
            sErrorDetails = [],
            oError = {},
            iStatus = jqXhr.status;

        if (sErrorType === "parsererror") {
            iStatus = 0;
            sErrorCode = "parsererror";
        } else if (jqXhr.responseJSON && jqXhr.responseJSON.error) {
            sErrorCode = jqXhr.responseJSON.error.code || "";
            sErrorMessage = jqXhr.responseJSON.error.message || "";
            sErrorDetails = jqXhr.responseJSON.error.details || [];
        }

        for (var i = 0; i < aHandlers.length; ++i) {
            var fnHandler = aHandlers[i];
            oError = fnHandler(jqXhr, sErrorCode, jqXhr.status);
            if (oError) {
                break;
            }
        }

        jQuery.sap.log.error("REST API call encountered an error of type '" + sErrorType + "' with HTTP status code "
            + jqXhr.status + ":\n" + JSON.stringify(jqXhr.responseText));

        return jQuery.extend({
            errorCode: sErrorCode,
            errorMessage: sErrorMessage,
            errorDetails: sErrorDetails,
            httpStatus: iStatus,
            jqXhr: jqXhr,
            silent: false
        }, oError);
    }

    function getOrCreateJSONModel(sModelName, sModelPathOrData) {
        var model = models[sModelName];
        if (!model) {
            model = models[sModelName] = new JSONModel(sModelPathOrData);
            model.setDefaultBindingMode("TwoWay");
            model.setSizeLimit(1000); // our max allowed top value
        }
        return model;
    }

    function loadData(oModel, sUrl, onSuccess, onError, fnPostProcessor) {
        var onDataLoaded = function(oData) {
            if (fnPostProcessor) {
                oData = fnPostProcessor(oData, oModel.getData());
            }
            oModel.setData(oData);
            if (onSuccess) {
                onSuccess(oData);
            }
        };
        return jQuery.ajax({
            type: "GET",
            url: sUrl,
            success: curry(handleSuccessAndSessionExpire, onDataLoaded, onError),
            error: function(jqXhr, sErrorType) {
                var oError = handleRESTError([], jqXhr, sErrorType);
                onError(oError);
            }
        });
    }

    var RESTEndPoints = {
        _uriWfsPrefix: "/wfs/v1",
        uriWorkflowDefinitions: function() {
            return this._uriWfsPrefix + "/workflow-definitions";
        },
        uriWorkflowInstances: function(oQueryParams) {
            var sUri = this._uriWfsPrefix + "/workflow-instances";
            if (oQueryParams) {
                sUri += "?" + jQuery.param(oQueryParams, true);
            }
            return sUri;
        },
        uriWorkflowInstance: function(instanceId) {
            return this.uriWorkflowInstances() + "/" + instanceId;
        },
        uriWorkflowInstanceMessages: function(instanceId) {
            return this.uriWorkflowInstance(instanceId) + "/error-messages";
        },
        uriWorkflowInstanceExecutionLogs: function(instanceId) {
            return this.uriWorkflowInstance(instanceId) + "/execution-logs";
        },
        uriXsrfToken: function() {
            return this._uriWfsPrefix + "/xsrf-token";
        }
    };

    var RESTEndPointsInternal = {
        _uriWfsPrefix: "/wfs/internal/v1",
        uriWorkflowDefinitions: function() {
            return this._uriWfsPrefix + "/workflow-definitions";
        },
        uriWorkflowDefinitionModel: function(sWorkflowDefinitionId) {
            return this.uriWorkflowDefinitions() + "/" + sWorkflowDefinitionId + "/model";
        }
    };

    var ModelAPI = EventProvider.extend("com.sap.bpm.monitorworkflow.Models", {

        attachWorkflowInstancesUpdated: function(oData, fnFunction, oListener) {
            this.attachEvent("workflowInstancesUpdated", oData, fnFunction, oListener);
            return this;
        },

        detachWorkflowInstancesUpdated: function(fnFunction, oListener) {
            this.detachEvent("workflowInstancesUpdated", fnFunction, oListener);
            return this;
        },

        fireWorkflowInstancesUpdated: function(mArguments) {
            this.fireEvent("workflowInstancesUpdated", mArguments);
            return this;
        },

        attachWorkflowDefinitionsUpdated: function(oData, fnFunction, oListener) {
            this.attachEvent("workflowDefinitionsUpdated", oData, fnFunction, oListener);
            return this;
        },

        detachWorkflowDefinitionsUpdated: function(fnFunction, oListener) {
            this.detachEvent("workflowDefinitionsUpdated", fnFunction, oListener);
            return this;
        },

        fireWorkflowDefinitionsUpdated: function(mArguments) {
            this.fireEvent("workflowDefinitionsUpdated", mArguments);
            return this;
        },

        getDeviceModel: function() {
            models.deviceModel = models.deviceModel || new JSONModel(Device);
            models.deviceModel.setDefaultBindingMode("OneWay");
            return models.deviceModel;
        },

        getCurrentWorkflowDefinitionModelUri: function() {
            var sWorkflowDefinitionId = this.getWorkflowDefinitionModel().getProperty("/id");
            return RESTEndPointsInternal.uriWorkflowDefinitionModel(sWorkflowDefinitionId);
        },

        getViewModel: function() {
            return getOrCreateJSONModel("viewModel", {
                "busy": {
                    "instancesList": false,
                    "definitionsList": false
                },
                "filter": {
                    "instancesList": "",
                    "definitionsList": ""
                },
                "count": {
                    "instancesList": 0,
                    "definitionsList": 0
                },
                "top": {
                    "instancesList": pageSize
                },
                "statusFilter": {
                    "instancesList": ["RUNNING", "ERRONEOUS", "SUSPENDED"]
                }
            });
        },

        getPageSize: function() {
            return pageSize;
        },

        getWorkflowDefinitionsModel: function() {
            return getOrCreateJSONModel("workflowDefinitionsModel");
        },

        updateWorkflowDefinitionsModel: function(onSuccess, onError) {
            var that = this;
            var model = this.getWorkflowDefinitionsModel();
            var viewModel = this.getViewModel();
            viewModel.setProperty("/busy/definitionsList", true);
            loadData(model, RESTEndPoints.uriWorkflowDefinitions(), onSuccess, onError).done(function(oData) {
                that.fireWorkflowDefinitionsUpdated({definitions: oData});
            }).always(function() {
                viewModel.setProperty("/busy/definitionsList", false);
            });
        },

        getWorkflowDefinitionModel: function() {
            models.workflowDefinitionModel = models.workflowDefinitionModel || new JSONModel();
            return models.workflowDefinitionModel;
        },

        updateWorkflowDefinitionModel: function(definition) {
            var model = this.getWorkflowDefinitionModel();
            model.setData(definition, false);
        },

        getWorkflowInstancesModel: function() {
            return getOrCreateJSONModel("workflowInstancesModel");
        },

        updateWorkflowInstancesModel: function(onSuccess, onError) {
            var filter = this.getViewModel().getProperty("/filter/instancesList");
            var top = this.getViewModel().getProperty("/top/instancesList");
            var statusFilterArray = this.getViewModel().getProperty("/statusFilter/instancesList");

            this._updateWorkflowInstancesModel(filter, top, statusFilterArray, onSuccess, onError);
        },

        _updateWorkflowInstancesModel: function(searchString, top, statusFilterArray, onSuccess, onError) {
            var that = this;
            var model = this.getWorkflowInstancesModel();
            var viewModel = this.getViewModel();
            viewModel.setProperty("/busy/instancesList", true);
            var oQueryParams = {$top: top, status: statusFilterArray, containsText: searchString};
            loadData(model, RESTEndPoints.uriWorkflowInstances(oQueryParams), onSuccess, onError).done(function(oData) {
                that.fireWorkflowInstancesUpdated({instances: oData});
            }).always(function() {
                viewModel.setProperty("/busy/instancesList", false);
            });
        },

        getWorkflowInstanceModel: function() {
            models.workflowInstanceModel = models.workflowInstanceModel || new JSONModel();
            return models.workflowInstanceModel;
        },

        _updateWorkflowInstanceModel: function(instanceId, onSuccess, onError) {
            var instanceModel = this.getWorkflowInstanceModel();
            var fnSingleResultPostProcessor = function(newData, oldData) {
                if (jQuery.isArray(newData) && newData.length === 1) {
                    return newData[0];
                } else {
                    // loading the instance did not return an array with exactly one result - so keep the original data
                    return oldData;
                }
            };
            loadData(instanceModel, RESTEndPoints.uriWorkflowInstances({id: instanceId}), onSuccess, onError, fnSingleResultPostProcessor);
        },

        updateWorkflowInstanceModel: function(instance, onSuccess, onError) {
            // 1. Already set the instance that was given so that there's the old data visible while it is refreshing
            this.setWorkflowInstanceModelData(instance);
            // 2. Reload the instance to ensure that the data is up to date
            this._updateWorkflowInstanceModel(instance.id, onSuccess, onError);
        },

        setWorkflowInstanceModelData: function(instance) {
            this.getWorkflowInstanceModel().setData(instance);
        },

        updateWorkflowInstanceErrorsModelToCurrentWorkflowInstance: function(onSuccess, onError) {
            var model = this.getWorkflowInstanceErrorsModel();
            var id = this.getWorkflowInstanceModel().getProperty("/id");
            model.setData([], false);
            loadData(model, RESTEndPoints.uriWorkflowInstanceMessages(id), onSuccess, onError);
        },

        updateWorkflowInstanceExecutionLogsModelToCurrentWorkflowInstance: function(fnPostProcessor, onSuccess, onError) {
            var model = this.getWorkflowInstanceExecutionLogsModel();
            var id = this.getWorkflowInstanceModel().getProperty("/id");
            model.setData([], false);
            loadData(model, RESTEndPoints.uriWorkflowInstanceExecutionLogs(id), onSuccess, onError, fnPostProcessor);
        },

        getWorkflowInstanceContextModel: function() {
            models.workflowInstanceContextModel = models.workflowInstanceContextModel || new JSONModel({context: ""});
            return models.workflowInstanceContextModel;
        },

        getWorkflowInstanceErrorsModel: function() {
            models.workflowInstanceErrorsModel = models.workflowInstanceErrorsModel || new JSONModel([]);
            return models.workflowInstanceErrorsModel;
        },

        getWorkflowInstanceExecutionLogsModel: function() {
            models.workflowInstanceExecutionLogsModel = models.workflowInstanceExecutionLogsModel || new JSONModel([]);
            return models.workflowInstanceExecutionLogsModel;
        },

        cancelCurrentWorkflowInstance: function(onSuccess, onError) {
            var id = this.getWorkflowInstanceModel().getProperty("/id");
            this.setStatusOfWorkflowInstance(id, "canceled", onSuccess, onError);
        },

        runCurrentWorkflowInstance: function(onSuccess, onError) {
            var id = this.getWorkflowInstanceModel().getProperty("/id");
            this.setStatusOfWorkflowInstance(id, "running", onSuccess, onError);
        },

        suspendCurrentWorkflowInstance: function(onSuccess, onError) {
            var id = this.getWorkflowInstanceModel().getProperty("/id");
            this.setStatusOfWorkflowInstance(id, "suspended", onSuccess, onError);
        },

        setStatusOfWorkflowInstance: function(instanceId, sStatus, onSuccess, onError) {
            var oPayload = {status: sStatus};
            this.performRequestWithCSRFToken("PATCH", RESTEndPoints.uriWorkflowInstance(instanceId), oPayload, onSuccess, onError);
        },

        startNewWorkflowInstance: function(definitionId, context, onSuccess, onError) {
            var oPayload = {
                definitionId: definitionId,
                context: context
            };
            this.performRequestWithCSRFToken("POST", RESTEndPoints.uriWorkflowInstances(), oPayload, onSuccess, onError);
        },

        performRequestWithCSRFToken: function(sMethod, sUrl, oPayload, onSuccess, onError, bPreventCSRFRetry) {
            var that = this;
            if (!csrfToken && !bPreventCSRFRetry) {
                this.requestCSRFToken(
                    this.performRequestWithCSRFToken.bind(this, sMethod, sUrl, oPayload, onSuccess, onError, true), onError);
                return;
            }
            jQuery.ajax({
                type: sMethod,
                url: sUrl,
                success: curry(handleSuccessAndSessionExpire, onSuccess, onError),
                error: function(jqXhr, sErrorType) {
                    var oError = handleRESTError([
                        curry(
                            retryWithNewCSRFToken,
                            bPreventCSRFRetry,
                            that.performRequestWithCSRFToken.bind(that, sMethod, sUrl, oPayload, onSuccess, onError, true),
                            onError
                        )
                    ], jqXhr, sErrorType);
                    if (!oError.silent) {
                        onError(oError);
                    }
                },
                data: JSON.stringify(oPayload),
                contentType: "application/json"
            });
        },

        requestCSRFToken: function(onSuccess, onError) {
            var onCsrfTokenReceived = function(data, textStatus, jqXhr) {
                csrfToken = jqXhr.getResponseHeader("X-CSRF-Token");
                if (onSuccess) {
                    onSuccess();
                }
            };

            jQuery.ajax({
                type: "GET",
                url: RESTEndPoints.uriXsrfToken(),
                headers: {
                    "X-CSRF-Token": "Fetch"
                },
                success: curry(handleSuccessAndSessionExpire, onCsrfTokenReceived, onError),
                error: function(jqXhr, sErrorType) {
                    onError(handleRESTError([], jqXhr, sErrorType));
                },
                contentType: "application/json"
            });
        }
    });

    // Create essentially a singleton instance of this class
    modelAPISingleton = new ModelAPI();
    return modelAPISingleton;
});