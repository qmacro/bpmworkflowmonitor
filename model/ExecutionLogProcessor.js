sap.ui.define([
    "com/sap/bpm/monitorworkflow/model/Models", "com/sap/bpm/monitorworkflow/utils/Utils",
    "com/sap/bpm/monitorworkflow/utils/i18n"
], function(Models, Utils, I18n) {
    "use strict";

    var oDateTimeFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
        pattern: "yyyy-MM-ddTHH:mm:ss.SSSZ"
    });

    var getActivityIdentifier = function(oTask) {
        return oTask.activityId + "__" + oTask.referenceInstanceId;
    };

    var aWhitelistedEventTypes = [
        "WORKFLOW_STARTED", "WORKFLOW_COMPLETED", "WORKFLOW_CANCELED", "WORKFLOW_SUSPENDED",
        "WORKFLOW_CONTINUED", "WORKFLOW_RESUMED", "WORKFLOW_CONTEXT_OVERWRITTEN_BY_ADMIN",
        "WORKFLOW_CONTEXT_PATCHED_BY_ADMIN", "USERTASK_CREATED", "USERTASK_CLAIMED", "USERTASK_RELEASED",
        "USERTASK_CANCELED_BY_BOUNDARY_EVENT", "USERTASK_COMPLETED", "USERTASK_FAILED", "SERVICETASK_CREATED",
        "SERVICETASK_COMPLETED", "SERVICETASK_FAILED", "SCRIPTTASK_CREATED", "SCRIPTTASK_COMPLETED",
        "SCRIPTTASK_FAILED", "INTERMEDIATE_MESSAGE_EVENT_REACHED", "INTERMEDIATE_MESSAGE_EVENT_TRIGGERED",
        "INTERMEDIATE_TIMER_EVENT_REACHED", "INTERMEDIATE_TIMER_EVENT_TRIGGERED",
        "CANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED", "NONCANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED",
        "USERTASK_PATCHED_BY_ADMIN"
    ];

    function keepOnlyWhitelistedEvents(oData) {
        Utils.arrayFilterInPlace(oData, function(oEvent) {
            return Utils.arrayIncludes(aWhitelistedEventTypes, oEvent.type);
        });
    }

    function removeActivityRetryEntries(oData) {
        // In place and in ascending order of timestamp:
        // Remove all script/service task CREATED events if there was already a CREATED event for the same task
        // previously and no COMPLETED or WORKFLOW_CONTINUED event since then.
        // Also remove all script/service task FAILED events if they have a retry counter that is greater than 0.
        var i, sIdentifier, oActivities = {};
        for (i = oData.length - 1; i >= 0; --i) {
            if (oData[i].type === "SCRIPTTASK_CREATED" || oData[i].type === "SERVICETASK_CREATED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                if (oActivities[sIdentifier] !== undefined) {
                    oData.splice(i, 1);
                } else {
                    oActivities[sIdentifier] = true;
                }
            } else if (oData[i].type === "SCRIPTTASK_COMPLETED" || oData[i].type === "SERVICETASK_COMPLETED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                delete oActivities[sIdentifier];
            } else if (oData[i].type === "WORKFLOW_CONTINUED") {
                oActivities = {};
            } else if (oData[i].type === "SCRIPTTASK_FAILED" || oData[i].type === "SERVICETASK_FAILED"
                || oData[i].type === "USERTASK_FAILED") {
                if (oData[i].retriesRemaining && oData[i].retriesRemaining > 0) {
                    oData.splice(i, 1);
                }
            }
        }
    }

    function keepOnlyLatestFailedOrCompletedEventPerActivity(oData) {
        // In place and in descending order of timestamp:
        // Remove all script/service task FAILED/COMPLETED events except the latest one for each CREATED event.
        var i, sIdentifier, oActivities = {};
        for (i = 0; i < oData.length; ++i) {
            if (oData[i].type === "SCRIPTTASK_CREATED" || oData[i].type === "SERVICETASK_CREATED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                delete oActivities[sIdentifier];
            } else if (oData[i].type === "SCRIPTTASK_FAILED" || oData[i].type === "SERVICETASK_FAILED"
                || oData[i].type === "SCRIPTTASK_COMPLETED" || oData[i].type === "SERVICETASK_COMPLETED"
                || oData[i].type === "USERTASK_FAILED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                if (oActivities[sIdentifier]) {
                    oData.splice(i, 1);
                    --i;
                } else {
                    oActivities[sIdentifier] = true;
                }
            }
        }
    }

    function calculateExecutionTime(sStartTimestamp, sEndTimestamp) {
        try {
            var iEndTimeInMs = oDateTimeFormat.parse(sEndTimestamp, true, true);
            var iStartTimeInMs = oDateTimeFormat.parse(sStartTimestamp, true, true);
            if (iEndTimeInMs && iStartTimeInMs) {
                return I18n.getText("DURATION_MILLISECONDS", iEndTimeInMs - iStartTimeInMs);
            }
        } catch (err) {
            jQuery.sap.log.error("Error parsing execution log timestamps: " + err);
        }
        return "";
    }

    function formatAndSubtractDurationUnit(iDurationInMs, unitLabel, msPerUnit) {
        var iDurationInUnit = Math.floor(iDurationInMs / msPerUnit);
        var formattedTime = "";
        if (iDurationInUnit >= 1) {
            formattedTime = I18n.getText("DURATION_" + unitLabel, iDurationInUnit);
        }
        return {
            formattedTime: formattedTime,
            remainingTime: iDurationInMs - iDurationInUnit * msPerUnit
        };
    }

    function calculateActualTimerDuration(sStartTimestamp, sEndTimestamp) {
        try {
            var iEndTimeInMs = oDateTimeFormat.parse(sEndTimestamp, true, true);
            var iStartTimeInMs = oDateTimeFormat.parse(sStartTimestamp, true, true);
            if (iEndTimeInMs && iStartTimeInMs) {
                var unitData = [
                    {
                        label: "DAYS",
                        millis: 86400 * 1000
                    }, {
                        label: "HOURS",
                        millis: 3600 * 1000
                    }, {
                        label: "MINUTES",
                        millis: 60 * 1000
                    }, {
                        label: "SECONDS",
                        millis: 1000
                    }
                ];
                var unitSeparator = " ";

                var iTimeInMs = iEndTimeInMs - iStartTimeInMs;
                // we use the fact that map is still sequentially traversing the array (i.e. use side-effects)
                return unitData.map(function formatUnit(item) {
                    var partialFormatData = formatAndSubtractDurationUnit(iTimeInMs, item.label, item.millis);
                    iTimeInMs = partialFormatData.remainingTime;
                    return partialFormatData.formattedTime;
                }).filter(function(item) {
                    return item.length > 0;
                }).join(unitSeparator);
            }
        } catch (err) {
            jQuery.sap.log.error("Error parsing execution log timestamps: " + err);
        }
        return "";
    }

    function calculateServiceTaskExecutionDuration(oData) {
        var i, sIdentifier, oStartTime = {};
        for (i = oData.length - 1; i >= 0; --i) {
            if (oData[i].type === "SERVICETASK_CREATED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                oStartTime[sIdentifier] = oData[i].timestamp;
            } else if (oData[i].type === "SERVICETASK_COMPLETED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                if (oStartTime[sIdentifier]) {
                    oData[i].executionDurationText = calculateExecutionTime(oStartTime[sIdentifier], oData[i].timestamp);
                } else {
                    oData[i].executionDurationText = "";
                }
            }
        }
    }

    function calculateDefinedDuration(iso8601Duration) {
        var iso8601regex = /P((([0-9]*\.?[0-9]*)(Y))?(([0-9]*\.?[0-9]*)(M))?(([0-9]*\.?[0-9]*)(W))?(([0-9]*\.?[0-9]*)(D))?)?(T(([0-9]*\.?[0-9]*)(H))?(([0-9]*\.?[0-9]*)(M))?(([0-9]*\.?[0-9]*)(S))?)?/;
        var unitsPositionToLabelMap = {
            3: "YEARS",
            6: "MONTHS",
            9: "WEEKS",
            12: "DAYS",
            16: "HOURS",
            19: "MINUTES"
        };
        var isoFields = iso8601regex.exec(iso8601Duration);

        function translateUnitPart(unitPosition) {
            if (isoFields[unitPosition]) {
                return I18n.getText("DURATION_" + unitsPositionToLabelMap[unitPosition], isoFields[unitPosition]);
            }
            return "";
        }

        if (isoFields) {
            return Object.keys(unitsPositionToLabelMap).map(translateUnitPart).filter(function(item) {
                return item.length > 0;
            }).join(" ");
        }
        return iso8601Duration;
    }

    function calculateIntermediateTimerEventDuration(oData) {
        var i, sIdentifier, oStartTime = {};
        for (i = oData.length - 1; i >= 0; --i) {
            if (oData[i].type === "INTERMEDIATE_TIMER_EVENT_REACHED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                oStartTime[sIdentifier] = oData[i].timestamp;
                oData[i].duration = calculateDefinedDuration(oData[i].duration);
            } else if (oData[i].type === "INTERMEDIATE_TIMER_EVENT_TRIGGERED") {
                sIdentifier = getActivityIdentifier(oData[i]);
                if (oStartTime[sIdentifier]) {
                    oData[i].duration = calculateActualTimerDuration(oStartTime[sIdentifier], oData[i].timestamp);
                } else {
                    oData[i].duration = "";
                }
            }
        }
    }

    function calculateBoundaryTimerEventDuration(oData) {
        var userTaskCreationTimes = oData.filter(function(logEntry) {
            return logEntry.type === "USERTASK_CREATED";
        }).reduce(function(accumulator, logEntry) {
            accumulator[logEntry.referenceInstanceId] = logEntry.timestamp;
            return accumulator;
        }, {});

        oData.forEach(function(logEntry) {
            if (jQuery.sap.endsWith(logEntry.type, "BOUNDARY_TIMER_EVENT_TRIGGERED")) {
                logEntry.duration =
                    calculateActualTimerDuration(userTaskCreationTimes[logEntry.referenceInstanceId], logEntry.timestamp);
            }
        });
    }

    return {
        ungrouped: function(oData, bSortDescending) {
            var sSortDirection = bSortDescending ? "desc" : "asc";
            oData.sort(Utils.sortByProperties(["timestamp", sSortDirection], ["id", sSortDirection]));
            keepOnlyWhitelistedEvents(oData);
        },

        basicGrouping: function(oData, bSortDescending) {
            oData.sort(Utils.sortByProperties(["timestamp", "desc"], ["id", "desc"]));

            // Remove unwanted entries from the log
            keepOnlyWhitelistedEvents(oData);
            removeActivityRetryEntries(oData);
            keepOnlyLatestFailedOrCompletedEventPerActivity(oData);

            // Calculate additional information
            calculateServiceTaskExecutionDuration(oData);
            calculateIntermediateTimerEventDuration(oData);
            calculateBoundaryTimerEventDuration(oData);

            // Sort ascending instead of descending, if requested
            if (!bSortDescending) {
                oData.reverse();
            }
        },

        fullGrouping: function(oData/* , bSortDescending */) {
            return oData;
        },

        validate: function(oData) {
            return Utils.arrayFind(oData, function(oEntry) {
                return oEntry.type === "WORKFLOW_STARTED";
            }) !== undefined;
        },

        getWhitelistedEventTypes: function() {
            return aWhitelistedEventTypes;
        }
    };

});
