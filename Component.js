sap.ui.define([
    "sap/ui/core/UIComponent",
    "com/sap/bpm/monitorworkflow/model/Models"
], function(UIComponent, Models) {
    "use strict";

    return UIComponent.extend("com.sap.bpm.monitorworkflow.Component", {

        metadata: {
            manifest: "json"
        },

        init: function() {
            // Call the init function of the parent
            UIComponent.prototype.init.apply(this, arguments);

            // Load custom fonts for custom icons
            jQuery.sap.require("com.sap.bpm.monitorworkflow.fonts.bootstrap-fonts");

            // Create the views based on the url/hash
            this.getRouter().initialize();

            // Initialize all models on the component so they are available centrally
            this.setModel(Models.getDeviceModel(), "device");
            this.setModel(Models.getViewModel(), "view");
            this.setModel(Models.getWorkflowDefinitionsModel(), "wfds");
            this.setModel(Models.getWorkflowDefinitionModel(), "wfd");
            this.setModel(Models.getWorkflowInstancesModel(), "wfis");
            this.setModel(Models.getWorkflowInstanceModel(), "wfi");
            this.setModel(Models.getWorkflowInstanceContextModel(), "wfictx");
            this.setModel(Models.getWorkflowInstanceErrorsModel(), "wfierr");
            this.setModel(Models.getWorkflowInstanceExecutionLogsModel(), "wfilog");

            // Navigate to targets based on actions in intents if running in FLP
            var urlSvc = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService &&
                sap.ushell.Container.getService("URLParsing");
            if (urlSvc) {
                var hash = urlSvc.parseShellHash(window.location.hash) || {};
                var action = hash.action || "DisplayInstances";
                var appSpecificRoute = hash.appSpecificRoute;

                if (appSpecificRoute) {
                    return;
                }

                if (action === "DisplayDefinitions") {
                    this.getRouter().navTo("definitions", null, true);
                } else {
                    this.getRouter().navTo("instances", null, true);
                }
            } else if (!window.location.hash) {
                this.getRouter().navTo("instances", null, true);
            }
        }
    });
});