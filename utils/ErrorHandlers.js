sap.ui.define([
    "sap/m/MessageBox",
    "com/sap/bpm/monitorworkflow/utils/Curry",
    "com/sap/bpm/monitorworkflow/utils/i18n",
    "com/sap/bpm/monitorworkflow/utils/Utils"
], function(MessageBox, curry, I18n, Utils) {
    "use strict";

    function csrfValidationFailed(jqXhr) {
        return jqXhr.status === 403 && jqXhr.getResponseHeader("X-CSRF-Token") === "Required";
    }

    var ErrorHandlers = {

        /** Takes a mix of functions and arrays of functions as variable arguments
         * and returns an array of error handlers that can be used with handle(aHandlers, oError).
         * An automatic fallback handler is added so that no error remains unhandled
         * @returns {Array}
         */
        create: function() {
            var aHandlers = [];
            var addFunction = function(fnHandler) {
                if (typeof fnHandler !== "function") {
                    jQuery.debug.log.error("[ErrorHandlers::create] Expecting function or array of functions");
                } else {
                    aHandlers.push(fnHandler);
                }
            };
            for (var i = 0; i < arguments.length; ++i) {
                var vHandler = arguments[i];
                if (Array.isArray(vHandler)) {
                    vHandler.forEach(addFunction);
                } else {
                    addFunction(vHandler);
                }
            }
            aHandlers.push(ErrorHandlers.fallback);
            return aHandlers;
        },

        handle: function(aHandlers, oError) {
            Utils.arrayFind(aHandlers, function(oHandler) {
                return oHandler(oError);
            });
        },

        handleSilentError: function(oError) {
            return oError.silent;
        },

        handleInvalidCSRFToken: function(oError) {
            if (csrfValidationFailed(oError.jqXhr)) {
                MessageBox.error(I18n.getText("ERROR_CSRF_VALIDATION"));
                return true;
            }
        },

        handleDefinitionNotFound: function(oError) {
            if (oError.errorCode === "bpm.workflowruntime.definition.not.found") {
                MessageBox.error(I18n.getText("ERROR_DEFINITION_NOT_FOUND"));
                return true;
            }
        },

        handleInstanceNotFound: function(sAction, oError) {
            if (oError.errorCode === "bpm.workflowruntime.instance.not.found") {
                MessageBox.error(I18n.getText("ERROR_" + sAction + "_FAILED_NOT_FOUND"));
                return true;
            }
        },

        handleOperationFailed: function(sAction /*, oError */) {
            MessageBox.error(I18n.getText("ERROR_" + sAction + "_FAILED_GENERIC"));
            return true;
        },

        handleSessionTimeout: function(oError) {
            if (oError.errorCode === "bpm.wfadmin.session.expired") {
                MessageBox.alert(I18n.getText("ERROR_SESSION_TIMEOUT"), {
                    styleClass: "sapUiSizeCompact",
                    onClose: function() {
                        location.reload();
                    }
                });
                return true;
            }
        },

        handleNoPermission: function(oError) {
            if (oError.httpStatus === 403) {
                MessageBox.error(I18n.getText("ERROR_NOT_ALLOWED"));
                return true;
            }
        },

        handleInternalServerError: function(oError) {
            if (oError.errorCode === "bpm.workflowruntime.internal.server.error" || oError.httpStatus === 500) {
                MessageBox.error(I18n.getText("ERROR_INTERNAL_ERROR"));
                return true;
            }
        },

        fallback: function(oError) {
            MessageBox.error(I18n.getText("ERROR_INTERNAL_ERROR"));
            return true;
        }

    };

    ErrorHandlers.defaultErrorHandlers = [
        ErrorHandlers.handleSilentError,
        ErrorHandlers.handleDefinitionNotFound,
        ErrorHandlers.handleSessionTimeout,
        ErrorHandlers.handleInvalidCSRFToken,
        ErrorHandlers.handleNoPermission,
        ErrorHandlers.handleInternalServerError
    ];

    ErrorHandlers.handleDefault = curry(
        ErrorHandlers.handle,
        ErrorHandlers.create(ErrorHandlers.defaultErrorHandlers, ErrorHandlers.fallback)
    );

    return ErrorHandlers;
});