# App Descriptor
appTitle=Monitor Workflows
appDescription=Monitor workflow definitions and workflow instances

##
# Definitions Master View
##

#XTIT: Title of the master view showing all definitions with count in brackets
DEFINITIONS_MASTER_HEADER=Workflow Definitions ({0})

#XMSG: Text below filter element indicating that provided filter condition does not match any definition title
DEFINITIONS_MASTER_NOT_FOUND=No definitions found.

##
# Definitions Detail View
##

#XTIT: Title of the detail view showing the selected definition
DEFINITIONS_DETAIL_HEADER=Workflow Definition

#XHED: Heading of the definition ID
DEFINITIONS_DEF_ID=ID

#XHED: Heading of the definition version
DEFINITIONS_DEF_VERSION=Version

#XBUT: Label of the Start Instance button to start a new instance based on a workflow definition; also used for title and button of the subsequent dialog
DEFINITIONS_START_INSTANCE=Start New Instance

#XBUT: Label of the Show Instances button to list all instances for a given workflow definition
DEFINITIONS_SHOW_INSTANCES=Show Instances

#XBUT: Label of the Download Workflow Model button which triggers browser download of the model JSON file
DEFINITIONS_DOWNLOAD_MODEL=Download Workflow Model

#XTXT: Descriptive text giving advice to provide context data to be used when starting the new instance
DEFINITIONS_START_INSTANCE_DESCRIPTION=Enter the JSON context with which to start the new instance:

#XBUT: Label of the button to cancel the Start New Instance dialog without taking any further action
DEFINITIONS_START_INSTANCE_CLOSE=Cancel

#XMSG: Message in MessageToast reporting that the given instance was successfully started
DEFINITIONS_DETAIL_STARTED_SUCCESS=Workflow instance started.

#XMSG: Warning that is displayed when the user might mistakenly include a wrapper object around his context. Do not translate the word context.
DEFINITIONS_CONTEXT_WARNING=Property ''context'' is not required as a wrapper.


##
# Instances Views (both, master and detail)
##

#XTXT: Identifies the instance with its definition name (first placeholder) and its start date+time (second placeholder full timestamp) , e.g. Leave Request started at Dec 1, 2016, 12:59:59 AM
INSTANCES_WORKFLOW_NAME={0} started at {1}

#XTXT: Status text reflecting the instance status Running
INSTANCES_WORKFLOW_STATUS_RUNNING=Running

#XTXT: Status text reflecting the instance status Erroneous
INSTANCES_WORKFLOW_STATUS_ERRONEOUS=Erroneous

#XTXT: Status text reflecting the instance status Suspended
INSTANCES_WORKFLOW_STATUS_SUSPENDED=Suspended

#XTXT: Status text reflecting the instance status Completed
INSTANCES_WORKFLOW_STATUS_COMPLETED=Completed

#XTXT: Status text reflecting the instance status Canceled
INSTANCES_WORKFLOW_STATUS_CANCELED=Canceled

#XTXT: Status text reflecting the instance status Undefined (fallback)
INSTANCES_WORKFLOW_STATUS_UNDEFINED=Undefined


##
# Instances Master View
##

#XTIT: Title of the master view showing all instances with count in brackets
INSTANCES_MASTER_HEADER=Workflow Instances ({0})

#XMSG: Text below filter element indicating that provided filter condition does not match any instance title
INSTANCES_MASTER_NOT_FOUND=No instances found.

#XBUT: Text for button to load more entries (workflow instances) 
INSTANCES_MASTER_BUTTON_MORE=More


##
# Instances Detail View
##

#XTIT: Title of the detail view showing the selected definition
INSTANCES_DETAIL_HEADER=Workflow Instance

#XBUT: Action text for the refresh button of the instances detail page
INSTANCES_DETAIL_REFRESH_ACTION=Refresh

#XHED: Heading of the instance: which user has started the instance
INSTANCES_DETAIL_STARTED_BY=Started By

#XHED: Heading of the instance: the business key of the instance
INSTANCES_DETAIL_BUSINESS_KEY=Business Key

#XHED: Heading of the instance: at which time the instance was started
INSTANCES_DETAIL_STARTED_AT=Started At

#XHED: Heading of the instance: the lifecycle status
INSTANCES_DETAIL_STATUS=Status

#XHED: Heading of the instance details pointing to the workflow information
INSTANCES_DETAIL_INFORMATION_HEADER=Workflow Information

#XHED: Heading of the instance details pointing to the error messages
INSTANCES_DETAIL_ERROR_MESSAGES_HEADER=Error Messages

#XHED: Heading of the instance details pointing to the error messages with count
INSTANCES_DETAIL_ERROR_MESSAGES_HEADER_WITH_COUNT=Error Messages ({0})

#XHED: Heading of the instance details pointing to the execution log section
INSTANCES_DETAIL_EXECUTION_LOG_HEADER=Execution Log

#XHED: Heading of the instance ID
INSTANCES_DETAIL_ID=Instance ID

#XHED: Heading of the definition ID
INSTANCES_DETAIL_DEF_ID=Definition ID

#XHED: Heading of the definition version
INSTANCES_DETAIL_DEF_VERSION=Definition Version

#XMSG: Text indicating that given instance does not have any error messages
INSTANCES_DETAIL_NO_ERRORS_FOUND=No error messages.

#XCOL: Heading of the error messages for column providing the error message
INSTANCES_DETAIL_ERRORS_MESSAGE=Message

#XCOL: Heading of the error messages for column providing the affected BPMN activity name
INSTANCES_DETAIL_ERRORS_ACTIVITY=Activity

#XCOL: Heading of the error messages for column providing the timestamp (date + time)
INSTANCES_DETAIL_ERRORS_TIMESTAMP=Timestamp

#XBUT: Text for the Retry button: recovers the instance by retrying the failed action again
INSTANCES_DETAIL_BUTTON_RETRY=Retry

#XBUT: Text for the Resume button: resumes a suspended workflow instance
INSTANCES_DETAIL_BUTTON_RESUME=Resume

#XBUT: Text for the Suspend button: suspends a running workflow instance
INSTANCES_DETAIL_BUTTON_SUSPEND=Suspend

#XBUT: Text for the Cancel button: cancels/terminates the instance
INSTANCES_DETAIL_BUTTON_CANCEL=Terminate

#XMSG: Confirmation message when attempting to cancel a workflow instance
INSTANCES_DETAIL_CANCEL_CONFIRMATION_MESSAGE=Terminate workflow instance ''{0}''?

#XTIT: Title of the confirmation dialog when attempting to cancel a workflow instance
INSTANCES_DETAIL_CANCEL_CONFIRMATION_TITLE=Terminate

#XBUT: Button label that confirms cancelling a workflow instance
INSTANCES_DETAIL_CANCEL_CONFIRMATION_CONFIRM=Terminate

#XBUT: Button label that cancels cancelling a workflow instance
INSTANCES_DETAIL_CANCEL_CONFIRMATION_CLOSE=Cancel

#XMSG: Message in MessageToast reporting successful cancellation of the given instance
INSTANCES_DETAIL_CANCEL_SUCCESS=Workflow instance canceled.

#XMSG: Message in MessageToast reporting that the given instance's status was requested for retry. For example, the requested action schedules an erroneous instance for re-executing a failed asynchronous workflow action.
INSTANCES_DETAIL_RETRY_SUCCESS=Workflow instance requested for retry.

#XMSG: Message in MessageToast reporting that the workflow instance was successfully suspended.
INSTANCES_DETAIL_SUSPEND_SUCCESS=Workflow instance suspended.

#XMSG: Message in MessageToast reporting that the workflow instance was successfully resumed from suspension (however, it might still be in erroneous state).
INSTANCES_DETAIL_RESUME_SUCCESS=Workflow instance resumed.


##
# Tooltips
##

#XTOL: Tooltip for the refresh button of the definitions master list
TOOLTIP_REFRESH_DEFINITIONS_LIST=Refresh Workflow Definitions

#XTOL: Tooltip for the search/filter button of the definitions master list
TOOLTIP_SEARCH_DEFINITIONS_LIST=Search Workflow Definitions

#XTOL: Tooltip for the refresh button of the instances master list
TOOLTIP_REFRESH_INSTANCES_LIST=Refresh Workflow Instances

#XTOL: Tooltip for the refresh button of the instances detail
TOOLTIP_REFRESH_INSTANCE=Refresh Workflow Instance

#XTOL: Tooltip for the search/filter button of the instances master list
TOOLTIP_SEARCH_INSTANCES_LIST=Search Workflow Instances

##
# Error dialogs
##

#XBUT: The button that offers the user the option to correct his previous input after an error
ERROR_CONTEXT_INVALID_CORRECT_INPUT=Correct

#XBUT: Button label that closes the error dialog after submitting an invalid workflow context object
ERROR_CONTEXT_INVALID_CANCEL=Close

##
# General Error Messages
##

#XMSG: Error message when starting a workflow instance and the definition id no longer exists
ERROR_DEFINITION_NOT_FOUND=The workflow instance could not be started because the workflow definition was not found. Refresh the list of workflow definitions.

#XMSG: Error message when starting a workflow instance with a workflow context object that is not accepted
ERROR_CONTEXT_INVALID=The JSON context contains errors. Check the details and correct the JSON context accordingly.

#XMSG: Error message when starting a workflow instance with a workflow context that is too large
ERROR_CONTEXT_TOO_LARGE=The JSON context is too large. Consider shortening the JSON context.

#XMSG: Value state of a text field that is shown when the JSON content of the field does not parse
ERROR_LABEL_INVALID_JSON=Enter valid JSON.

#XMSG: Error message that the user session at the backend system has expired. Reloading the page will create a new user session.
ERROR_SESSION_TIMEOUT=The session timed out. Confirm reloading the page.

#XMSG: Error message that the user has no permission; backend has responded with HTTP 403.
ERROR_NOT_ALLOWED=The operation could not be performed due to missing permission.

#XMSG: Error message that in backend side an HTTP 500 Internal Server Error occurred
ERROR_INTERNAL_ERROR=An error occurred while processing your request. Retry later or contact the service provider.

#XMSG: Error message when CSRF validation failed which can be an issue with the application or the backend
ERROR_CSRF_VALIDATION=An error occurred while processing your request. Retry later or contact the service provider.

#XMSG: Error message when canceling a workflow instance failed because the workflow instance's status does not allow it.
ERROR_CANCEL_WORKFLOW_FAILED_NOT_FOUND=The workflow instance could not be canceled because it is already terminated. Refresh the list of workflow instances.

#XMSG: Error message when canceling a workflow instance failed due to any kind of error
ERROR_CANCEL_WORKFLOW_FAILED_GENERIC=The workflow instance could not be canceled. Retry later or contact the service provider.

#XMSG: Error message when retrying a workflow instance failed because the workflow instance does not exist
ERROR_RETRY_WORKFLOW_FAILED_NOT_FOUND=The workflow instance could not be retried because it is already terminated. Refresh the list of workflow instances.

#XMSG: Error message when retrying a workflow instance failed due to any kind of error
ERROR_RETRY_WORKFLOW_FAILED_GENERIC=The workflow instance could not be retried. Retry later or contact the service provider.

#XMSG: Error message when suspending a workflow instance failed because the workflow instance does not exist
ERROR_SUSPEND_WORKFLOW_FAILED_NOT_FOUND=The workflow instance could not be suspended because it is already terminated. Refresh the list of workflow instances.

#XMSG: Error message when suspending a workflow instance failed due to any kind of error
ERROR_SUSPEND_WORKFLOW_FAILED_GENERIC=The workflow instance could not be suspended. Retry later or contact the service provider.

#XMSG: Error message when resuming a workflow instance failed because the workflow instance does not exist
ERROR_RESUME_WORKFLOW_FAILED_NOT_FOUND=The workflow instance could not be resumed because it is already terminated. Refresh the list of workflow instances.

#XMSG: Error message when resuming a workflow instance failed due to any kind of error
ERROR_RESUME_WORKFLOW_FAILED_GENERIC=The workflow instance could not be resumed. Retry later or contact the service provider.

#XMSG: Error message when a workflow instance was opened but its error message won't load for any reason
ERROR_GET_MESSAGES_FAILED_GENERIC=The error messages for the selected workflow instance could not be retrieved. Retry later or contact the service provider.

#XMSG: Error message when a workflow instance was opened but its execution logs won't load for any reason
ERROR_GET_EXECUTION_LOGS_FAILED_GENERIC=The execution logs for the selected workflow instance could not be retrieved. Retry later or contact the service provider.

#XMSG: Error message when a workflow instance won't load for any reason
ERROR_GET_INSTANCE_FAILED_GENERIC=The selected workflow instance could not be retrieved. Retry later or contact the service provider.
##
# User Security Consent Dialog
##

#XTIT: Title for dialog asking for user consent of saving sensitive data
SENSITIVE_DATA_CONSENT_DIALOG_TITLE=Warning

#XMSG: Message of dialog asking for user consent of saving sensitive data
SENSITIVE_DATA_CONSENT_DIALOG_MESSAGE=This file could contain sensitive data, which gets downloaded to the configured download location. Are you sure you want to proceed?

#XTXT: Option for "not showing again" for dialog asking for user consent of saving sensitive data
SENSITIVE_DATA_CONSENT_DIALOG_DO_NOT_SHOW_AGAIN=Do not show this message again for this session

#XBUT: Button to confirm dialog asking for user consent of saving sensitive data
SENSITIVE_DATA_CONSENT_DIALOG_OK=OK

#XBUT: Button to cancel dialog asking for user consent of saving sensitive data
SENSITIVE_DATA_CONSENT_DIALOG_CANCEL=Cancel

##
# Execution Log Messages
##

#XMSG: Message for the timeline control when the execution log is empty
EXECUTION_LOG_NO_ITEMS=No execution log entries available.

#XMSG: Message displayed in a MessageStrip when the log validation failed (i.e. the log is inconsistent)
EXECUTION_LOG_VALIDATION_FAILED=The execution log is incomplete because this workflow instance was started before recording the log. This has no impact on the execution of the workflow instance.

#XTIT: Timeline item title when a workflow was started by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_STARTED=started the workflow

#XTIT: Timeline item title when a workflow completed without errors.
EXECUTION_LOG_TYPE_WORKFLOW_COMPLETED=Workflow completed successfully

#XTIT: Timeline item title when a workflow was canceled by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_CANCELED=canceled the workflow

#XTIT: Timeline item title when workflow instance was suspended (inactivated). The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_SUSPENDED=suspended the workflow

#XTIT: Timeline item title when failed workflow jobs were retried by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_CONTINUED=retried the workflow

#XTIT: Timeline item title when a suspended workflow instance is resumed by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_RESUMED=resumed the workflow

#XTIT: Timeline item title when the workflow context is completely overwritten by an administrator. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_CONTEXT_OVERWRITTEN_BY_ADMIN=overwrote the workflow context

#XTIT: Timeline item title when the workflow context is partially modified by an administrator. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_WORKFLOW_CONTEXT_PATCHED_BY_ADMIN=changed the workflow context

#XTIT: Timeline item when a user task was created by the system. {0} contains the subject of the task.
EXECUTION_LOG_TYPE_USERTASK_CREATED=Task "{0}" available

#XTIT: Timeline item title when a user task was claimed by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_USERTASK_CLAIMED=claimed the task "{0}"

#XTIT: Timeline item title when a user task was released by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_USERTASK_RELEASED=released the task "{0}"

#XTIT: Timeline item title when a user task was canceled by a boundary event.
EXECUTION_LOG_TYPE_USERTASK_CANCELED_BY_BOUNDARY_EVENT=Task "{0}" canceled by boundary event

#XTIT: Timeline item title when a user task was completed by a user. The user name / user ID is prepended.
EXECUTION_LOG_TYPE_USERTASK_COMPLETED=completed the task "{0}"

#XTIT: Timeline item title when the context of a user task was patched by admin. The admin name / user ID is prepended.
EXECUTION_LOG_TYPE_USERTASK_PATCHED_BY_ADMIN=updated the details of task "{0}"

#XTIT: Timeline item title when a user task failed.
EXECUTION_LOG_TYPE_USERTASK_FAILED=Task "{0}" failed

#XTIT: Timeline item when a service task was created by the system. {0} contains the name of the task.
EXECUTION_LOG_TYPE_SERVICETASK_CREATED="{0}" started

#XTIT: Timeline item when a service task was completed by the system. {0} contains the name of the task.
EXECUTION_LOG_TYPE_SERVICETASK_COMPLETED="{0}" completed successfully

#XTIT: Timeline item when a service task failed. {0} contains the name of the task.
EXECUTION_LOG_TYPE_SERVICETASK_FAILED="{0}" failed

#XTIT: Timeline item when a script task was created by the system. {0} contains the name of the task.
EXECUTION_LOG_TYPE_SCRIPTTASK_CREATED="{0}" started

#XTIT: Timeline item when a script task was completed by the system. {0} contains the name of the task.
EXECUTION_LOG_TYPE_SCRIPTTASK_COMPLETED="{0}" completed successfully

#XTIT: Timeline item when a script task failed. {0} contains the name of the task.
EXECUTION_LOG_TYPE_SCRIPTTASK_FAILED="{0}" failed

#XTIT: Timeline item subject when an intermediate message event is waiting for a message. {0} contains the name of the event.
EXECUTION_LOG_TYPE_INTERMEDIATE_MESSAGE_EVENT_REACHED="{0}" is waiting for a message

#XMSG: Timeline item details when an intermediate message event is waiting for a message.
EXECUTION_LOG_TYPE_INTERMEDIATE_MESSAGE_EVENT_REACHED_TEXT=Message name:

#XTIT: Timeline item subject when an intermediate message event is completed successfully. {0} contains the name of the event.
EXECUTION_LOG_TYPE_INTERMEDIATE_MESSAGE_EVENT_TRIGGERED="{0}" completed successfully

#XMSG: Timeline item details when an intermediate message event is completed successfully.
EXECUTION_LOG_TYPE_INTERMEDIATE_MESSAGE_EVENT_TRIGGERED_TEXT=Received message:

#XTIT: Timeline item when an canceling boundary timer event is triggered. {0} contains the name of the user task the event is attached to.
EXECUTION_LOG_TYPE_CANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED=Task "{0}" reached specified time

#XTIT: Timeline item when an non-canceling boundary timer event is triggered. {0} contains the name of the user task the event is attached to.
EXECUTION_LOG_TYPE_NONCANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED=Task "{0}" reached specified time

#XTIT: Timeline item when an intermediate timer event is reached. {0} contains the name of the event.
EXECUTION_LOG_TYPE_INTERMEDIATE_TIMER_EVENT_REACHED="{0}" is waiting

#XTIT: Timeline item when a intermediate timer event is triggered. {0} contains the name of the event.
EXECUTION_LOG_TYPE_INTERMEDIATE_TIMER_EVENT_TRIGGERED="{0}" completed successfully

#XTIT: Title of the popover (on phones only) that displays all recipient users
EXECUTION_LOG_ALL_RECIPIENT_USERS_TITLE=All Recipient Users

#XTIT: Title of the popover (on phones only) that displays all recipient groups
EXECUTION_LOG_ALL_RECIPIENT_GROUPS_TITLE=All Recipient Groups

#XMSG: Label for the list of recipient users and groups
EXECUTION_LOG_RECIPIENTS_LABEL=Recipients:

#XTOL: Tooltip for the recipient users icon
EXECUTION_LOG_TOOLTIP_RECIPIENT_USERS=Users

#XTOL: Tooltip for the recipient groups icon
EXECUTION_LOG_TOOLTIP_RECIPIENT_GROUPS=Groups

#XMSG: Link text to access more users when there are too many to display them all at once.
EXECUTION_LOG_TYPE_USERTASK_CREATED_MORE_USERS=and {0} more

#XMSG: Label for the initiator
EXECUTION_LOG_INITIATOR_LABEL=Initiator:

#XMSG: Label for the canceling boundary timer event name
EXECUTION_LOG_CANCELING_BOUNDARY_TIMER_EVENT_NAME_LABEL=Canceling Boundary Timer Event Name:

#XMSG: Label for the non-canceling boundary timer event name
EXECUTION_LOG_NONCANCELING_BOUNDARY_TIMER_EVENT_NAME_LABEL=Non-Canceling Boundary Timer Event Name:

#XMSG: Label for the boundary timer event name
EXECUTION_LOG_BOUNDARY_TIMER_EVENT_NAME_LABEL=Boundary Timer Event Name:

#XMSG: Label for the context or subject update
EXECUTION_LOG_CHANGED_PROPERTIES_LABEL=Changed properties:

#XMSG: Label for the subject update
EXECUTION_LOG_SUBJECT_CHANGED_PROPERTIES_LABEL=New Subject:

#XMSG: Label for the duration
EXECUTION_LOG_DURATION_LABEL=Duration:

#XMSG: Label for the time passed
EXECUTION_LOG_TIME_PASSED_LABEL=Time Passed:

#XTOL: Tooltip for timeline icon for event type WORKFLOW_STARTED
EXECUTION_LOG_TOOLTIP_WORKFLOW_STARTED=Workflow started

#XTOL: Tooltip for timeline icon for event type WORKFLOW_COMPLETED
EXECUTION_LOG_TOOLTIP_WORKFLOW_COMPLETED=Workflow completed

#XTOL: Tooltip for timeline icon for event type WORKFLOW_CANCELED
EXECUTION_LOG_TOOLTIP_WORKFLOW_CANCELED=Workflow canceled

#XTOL: Tooltip for timeline icon for event type WORKFLOW_SUSPENDED
EXECUTION_LOG_TOOLTIP_WORKFLOW_SUSPENDED=Workflow suspended

#XTOL: Tooltip for timeline icon for event type WORKFLOW_CONTINUED
EXECUTION_LOG_TOOLTIP_WORKFLOW_CONTINUED=Workflow retried

#XTOL: Tooltip for timeline icon for event type WORKFLOW_RESUMED
EXECUTION_LOG_TOOLTIP_WORKFLOW_RESUMED=Workflow resumed

#XTOL: Tooltip for timeline icon for event type WORKFLOW_CONTEXT_OVERWRITTEN_BY_ADMIN
EXECUTION_LOG_TOOLTIP_WORKFLOW_CONTEXT_OVERWRITTEN_BY_ADMIN=Workflow context overwritten by administrator

#XTOL: Tooltip for timeline icon for event type WORKFLOW_CONTEXT_PATCHED_BY_ADMIN
EXECUTION_LOG_TOOLTIP_WORKFLOW_CONTEXT_PATCHED_BY_ADMIN=Workflow context partially modified by administrator

#XTOL: Tooltip for timeline icon for event type USERTASK_CREATED
EXECUTION_LOG_TOOLTIP_USERTASK_CREATED=User Task created

#XTOL: Tooltip for timeline icon for event type USERTASK_CLAIMED
EXECUTION_LOG_TOOLTIP_USERTASK_CLAIMED=User Task claimed

#XTOL: Tooltip for timeline icon for event type USERTASK_FAILED
EXECUTION_LOG_TOOLTIP_USERTASK_FAILED=User Task failed

#XTOL: Tooltip for timeline icon for event type USERTASK_RELEASED
EXECUTION_LOG_TOOLTIP_USERTASK_RELEASED=User Task released

#XTOL: Tooltip for timeline icon for event type USERTASK_CANCELED_BY_BOUNDARY_EVENT
EXECUTION_LOG_TOOLTIP_USERTASK_CANCELED_BY_BOUNDARY_EVENT=User Task canceled by Boundary Event

#XTOL: Tooltip for timeline icon for event type USERTASK_COMPLETED
EXECUTION_LOG_TOOLTIP_USERTASK_COMPLETED=User Task completed

#XTOL: Tooltip for timeline icon for event type USERTASK_PATCHED_BY_ADMIN
EXECUTION_LOG_TOOLTIP_USERTASK_PATCHED_BY_ADMIN=User Task details updated

#XTOL: Tooltip for timeline icon for event type SERVICETASK_CREATED
EXECUTION_LOG_TOOLTIP_SERVICETASK_CREATED=Service Task created

#XTOL: Tooltip for timeline icon for event type SERVICETASK_COMPLETED
EXECUTION_LOG_TOOLTIP_SERVICETASK_COMPLETED=Service Task completed

#XTOL: Tooltip for timeline icon for event type SERVICETASK_FAILED
EXECUTION_LOG_TOOLTIP_SERVICETASK_FAILED=Service Task failed

#XTOL: Tooltip for timeline icon for event type SCRIPTTASK_CREATED
EXECUTION_LOG_TOOLTIP_SCRIPTTASK_CREATED=Script Task created

#XTOL: Tooltip for timeline icon for event type SCRIPTTASK_COMPLETED
EXECUTION_LOG_TOOLTIP_SCRIPTTASK_COMPLETED=Script Task completed

#XTOL: Tooltip for timeline icon for event type SCRIPTTASK_FAILED
EXECUTION_LOG_TOOLTIP_SCRIPTTASK_FAILED=Script Task failed

#XTIT: Tooltip for timeline icon for event type INTERMEDIATE_MESSAGE_EVENT_REACHED
EXECUTION_LOG_TOOLTIP_INTERMEDIATE_MESSAGE_EVENT_REACHED=Intermediate Message Event reached

#XTIT: Tooltip for timeline icon for event type INTERMEDIATE_MESSAGE_EVENT_TRIGGERED
EXECUTION_LOG_TOOLTIP_INTERMEDIATE_MESSAGE_EVENT_TRIGGERED=Intermediate Message Event triggered

#XTIT: Tooltip for timeline icon for event type INTERMEDIATE_TIMER_EVENT_REACHED
EXECUTION_LOG_TOOLTIP_INTERMEDIATE_TIMER_EVENT_REACHED=Intermediate Timer Event reached

#XTIT: Tooltip for timeline icon for event type INTERMEDIATE_TIMER_EVENT_TRIGGERED
EXECUTION_LOG_TOOLTIP_INTERMEDIATE_TIMER_EVENT_TRIGGERED=Intermediate Timer Event triggered

#XTIT: Tooltip for timeline icon for event type CANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED
EXECUTION_LOG_TOOLTIP_CANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED=Canceling Boundary Timer Event triggered

#XTIT: Tooltip for timeline icon for event type NONCANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED
EXECUTION_LOG_TOOLTIP_NONCANCELING_BOUNDARY_TIMER_EVENT_TRIGGERED=Non-Canceling Boundary Timer Event triggered

#XMSG: Label in front of the error message of a service or script task
ACTIVITY_FAILURE_ERROR_LABEL=Error message:

#XMSG: Label in front of the destination name that was used by a service task
SERVICE_TASK_ENDPOINT_LABEL=Service endpoint:

#XMSG: Fallback when the destination name is not known (undefined or empty)
DESTINATION_NAME_UNKNOWN=unknown

#XMSG: A duration in milliseconds
DURATION_MILLISECONDS={0} ms

#XMSG: A duration in seconds, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_SECONDS={0} seconds

#XMSG: A duration in minutes, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_MINUTES={0} minutes

#XMSG: A duration in hours, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_HOURS={0} hours

#XMSG: A duration in days, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_DAYS={0} days

#XMSG: A duration in weeks, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_WEEKS={0} weeks

#XMSG: A duration in months, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_MONTHS={0} months

#XMSG: A duration in years, with unit (may be preceded or followed by other DURATION_* parts, separated by spaces)
DURATION_YEARS={0} years

#XMSG: Fallback when the recipient users of a task are not known (undefined or empty)
RECIPIENT_USERS_UNKNOWN=unknown

#XMSG: Fallback when the error message of a task is not known (undefined or empty)
ERROR_MESSAGE_UNKNOWN=unknown

#XMSG: Label for the event type
EVENT_TYPE_LABEL=Event Type

#XMSG: Label for the task ID
TASK_ID_LABEL=Task ID
